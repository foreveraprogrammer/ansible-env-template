# Purpose
Installs git role for MacOS with gitignore and gitconfig files

# Installing role
ansible-galaxy install -r requirements.yml


# Running
To run type ansible-playbook -K -c local -i inventory playbook.yml

